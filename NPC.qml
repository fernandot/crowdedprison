import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.8

Image{
    x:0
    y:1
    z:0
    source: "Images/PStanding.png"
    property bool alive: true;
    onAliveChanged: {
        if(!alive){
            walking1.visible=true;
            bloodAnim.start();
            rotation=90;
            mirror=false;
            y+=10;
            enabled=false;
            clickAudio.play();
            fadding.start();
            if(isEveryNPCDead()) scenario.end();
        }
    }
    function isEveryNPCDead()
    {
        for(var c = 0; c<scenario.children.length;c++){
            var child=scenario.children[c];
            if(child.toString().substring(0,3)==="NPC")
            if(child.alive)
                return false;
        }
        return true;
    }

    NumberAnimation on opacity {
        id: fadding
        from:1
        to:0
        duration: 10000
        easing.type: Easing.InExpo
        running: false
    }

    Audio{
        id:clickAudio
        source: "Audio/dieing.mp3"
    }

    Image{
        id: blood
        source: "Images/Blood.png"
        visible: !alive
        z:0

        NumberAnimation on scale {
            id:bloodAnim
            from: 1
            to:1.5;
            duration: 10000;
        }
    }
    Image{
        id: walking1
        anchors.fill: parent
        source: "Images/PWalking1.png"
        visible: false
        mirror:parent.mirror
    }
    Image{
        id: walking2
        anchors.fill: parent
        source: "Images/PWalking2.png"
        visible: false
        mirror:parent.mirror
    }
    Image{
        id: unarmedHand
        x:mirror?parent.width/2:7;
        y:parent.height/2
        source: "Images/unarmedHand.png"
        visible: true
        mirror:parent.mirror

        NumberAnimation on x {
                  loops: Animation.Infinite
                  from: mirror?24:10;
                  to: mirror?20:14;
                  duration: 500;
                  paused: !isWalking()||!alive;
              }
    }
    Image{
        id: armedHand
        x:mirror?parent.width/2:7;
        y:parent.height/2
        source: "Images/armedHand.png"
        visible: false
        mirror:parent.mirror

        NumberAnimation on x {
            id:attackAnim1
            from: mirror?30:0
            to:mirror?20:10;
            duration: 100;
            running: false;
            alwaysRunToEnd: true;
        }
    }

    rotation: 0
    property int velocity: 4
    property bool goRight: false
    property bool goLeft: false
    property bool goUp: false
    property bool goDown: false

    function attack(){
        if(alive)
            attackAnim1.start();
    }

    function isWalking(){
        return goRight||goLeft||goUp||goDown;
    }

    Timer
    {
        interval: 20
        running: alive
        repeat: alive
        property int count:0
        onTriggered: {
            x+=goRight?x<scenario.width-width?!scenario.colisionDetection(x+5+velocity,y+40,30,5)?velocity:0:0:0;
            x-=goLeft?x>0?!scenario.colisionDetection(x+5-velocity,y+40,30,5)?velocity:0:0:0;
            y+=goDown?y<scenario.height-height?!scenario.colisionDetection(x+5,y+height-5+velocity,30,5)?velocity:0:0:0;
            y-=goUp?y>0?!scenario.colisionDetection(x+5,y+height-5-velocity,30,5)?velocity:0:0:0;
            mirror=goRight?true:mirror;
            mirror=goLeft?false:mirror;
            count=count<velocity*5-1?count+1:0;
            if(count%(velocity*5)>velocity*5/2)
            {
                walking1.visible=isWalking();
                walking2.visible=false;
            }
            else
            {
                walking1.visible=false;
                walking2.visible=isWalking();
            }
            z=scenario.priorityDetection(x+5,y+height-5,30,5)?0:2;
        }
    }

    Timer
    {
        interval: 300
        running: true
        repeat: true
        property int movementDirection: Math.random()*10;
        onTriggered: {
            movementDirection = Math.random()*10;
            goRight=movementDirection===0;
            goLeft=movementDirection===1;
            goUp=movementDirection===2;
            goDown=movementDirection===3;
        }
    }
}
