import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.8

Image {
    z:1
    source: "Images/PrisonGate.png"

    property int bbx: x
    property int bby: y+height-10
    property int bbw: width
    property int bbh: 10

    property bool opened:false;
    NumberAnimation on x {
        id:openAnim
        from: x
        to:x+(opened?width:-width);
        duration: 1000;
        easing.type: Easing.InExpo;
        running: false;
        alwaysRunToEnd: true
    }
    function open() {
        if(!openAnim.running)
        {
            openAnim.start();
            opened=!opened;
            audio.play();
        }
    }

    Audio{
        id:audio
        source: "Audio/gate.mp3"
    }

    Image {
        id:overlayImg
        anchors.fill: parent
        source: "Images/PrisonGate.png"
        visible: false
    }

    ColorOverlay{
        anchors.fill:parent
        source: overlayImg
        color: "#50800000"
        visible: openAnim.running
    }
}
