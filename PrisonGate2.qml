import QtQuick 2.0
import QtGraphicalEffects 1.0

Image {
    z:1
    source: "Images/PrisonGate2.png"

    property int bbx: x
    property int bby: y
    property int bbw: width
    property int bbh: height

    property bool opened:false;
    NumberAnimation on y {
        id:openAnim
        from: y
        to:y+(opened?height-10:10-height);
        duration: 1000;
        easing.type: Easing.InExpo;
        running: false;
        alwaysRunToEnd: true
    }
    function open() {
       /* if(!openAnim.running)
        {
            openAnim.start();
            opened=!opened;
        }*/
    }

    Image {
        id:overlayImg
        anchors.fill: parent
        source: "Images/PrisonGate2.png"
        visible: false
    }

    ColorOverlay{
        anchors.fill:parent
        source: overlayImg
        color: "#50800000"
        visible: openAnim.running
    }
}
