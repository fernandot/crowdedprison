import QtQuick 2.0

Image {
    z:1
    source: "Images/PrisonWall.png"
    property int bbx: x
    property int bby: y+height-10
    property int bbw: width
    property int bbh: 10
}
