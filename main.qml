import QtQuick 2.9
import QtQuick.Window 2.2
import QtMultimedia 5.8

Window {
    visible: true
    width: 640
    height: 480
    maximumWidth: 640
    maximumHeight: 480
    minimumWidth: 640
    minimumHeight: 480
    title: qsTr("Crowded Prison")
    Rectangle {
        id: scenario
        anchors.fill: parent
        border.color: "black"
        gradient: Gradient {
            GradientStop {
                position: 0.00;
                color: "#dbdbcb";
            }
            GradientStop {
                position: 1.00;
                color: "#b6b795";
            }
        }

        function colisionDetection(x,y,w,h){
            for(var c = 0; c<scenario.children.length;c++){
                var child=scenario.children[c];
                if(child.bbx)
                if(x+w>child.bbx&&x<child.bbx+child.bbw&&
                        y+h>child.bby&&y<child.bby+child.bbh)
                    return true;
            }
            return false;
        }

        function priorityDetection(x,y,w,h){
            for(var c = 0; c<scenario.children.length-2;c++){
                var child=scenario.children[c];
                if(child.toString().substring(0,6)!=="Player"&&
                        child.toString().substring(0,3)!=="NPC")
                if(x+w>child.x&&x<child.x+child.width&&
                        y+h>child.y&&y<child.y+child.height)
                    return true;
            }
            return false;
        }

        //Top
        PrisonWall{
            x:1
            y:100
        }
        PrisonGate {
            x:160
            y:100
        }
        PrisonWall {
            x:220
            y:100
        }
        PrisonWall2 {
            x:240
            y:0
        }
        PrisonGate {
            x:380
            y:100
        }
        PrisonWall {
            x:440
            y:100
        }
        PrisonWall2 {
            x:460
            y:0
        }

        //Bottom
        PrisonWall{
            x:1
            y:320
        }
        PrisonGate {
            x:160
            y:320
        }
        PrisonWall {
            x:220
            y:320
        }
        PrisonWall3 {
            x:240
            y:322
        }
        PrisonGate {
            x:380
            y:320
        }
        PrisonWall {
            x:440
            y:320
        }
        PrisonWall3 {
            x:460
            y:322
        }

        //Middle
        PrisonWall4 {
            x:540
            y:102
        }
        PrisonWall5 {
            x:540
            y:260
        }
        PrisonGate2 {
            x:550
            y:196
        }

        Player {}
        NPC {}
        NPC {}
        NPC {}
        NPC {}
        NPC {y:400}
        NPC {y:400}
        NPC {y:400}
        NPC {y:400}
        NPC {x:300;y:0}
        NPC {x:300;y:0}
        NPC {x:300;y:0}
        NPC {x:300;y:0}
        NPC {x:300;y:400}
        NPC {x:300;y:400}
        NPC {x:300;y:400}
        NPC {x:300;y:400}
        NPC {x:30;y:200}
        NPC {x:30;y:200}
        NPC {x:30;y:200}
        NPC {x:300;y:200}
        NPC {x:300;y:200}
        NPC {x:300;y:200}
        NPC {x:300;y:200}

        function end()
        {
            endingScreen.visible=true;
        }

        Rectangle{
            id:endingScreen
            anchors.fill:parent
            z:9
            visible: false
            Image{
                anchors.horizontalCenter: parent.horizontalCenter
                y:200
                scale:1.5
                source:"Images/TitleScreenJailed.png"
            }
            Text{
                anchors.horizontalCenter: parent.horizontalCenter
                y:10
                text:
"You've killed everyone. But you are still in jail. \n \
Soon the corpses will start to smell. \n \
You diserve it, 'cause you're a criminal! \n\n
At least now you have plenty of space."
            }
        }

        Rectangle{
            id:titleScreen
            anchors.fill:parent
            z:10
            Image{
                anchors.horizontalCenter: parent.horizontalCenter
                y:200
                scale:1.5
                source:"Images/TitleScreenJailed.png"
            }
            Text{
                anchors.horizontalCenter: parent.horizontalCenter
                y:10
                text:
"You are a psychopath, who doesn't like crowd places\n \
and the authorities sent you for the most populated jail.\n
It is recreation time, so the cell doors are unlocked and \n \
you just found a piercy metal capable of stabbing. \n
Controls: \n \
W: Change armed/unarmed \n \
Space: Attack/Opens doors \n \
Arrows: Move \n\n
Have Fun!"
            }
            Rectangle{
                anchors.horizontalCenter: parent.horizontalCenter
                y:350
                width: 50
                height: 20
                border.color: "#fff1f1"
                gradient: Gradient {
                    GradientStop {
                        position: 0.00;
                        color: "#74eac5";
                    }
                    GradientStop {
                        position: 1.00;
                        color: "#2184b5";
                    }
                }
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Play")
                }
                Audio{
                    id:clickAudio
                    source: "Audio/click.mp3"
                    onStopped: titleScreen.visible=false;
                }

                MouseArea{
                    anchors.fill: parent
                    onPressed: {
                        clickAudio.play();
                    }
                }
            }
        }

    }
}
