import QtQuick 2.0

Image{
    id: player
    x:0
    y:1
    z:0
    source: "Images/PStanding.png"
    Image{
        id: walking1
        anchors.fill: parent
        source: "Images/PWalking1.png"
        visible: false
        mirror:player.mirror
    }
    Image{
        id: walking2
        anchors.fill: parent
        source: "Images/PWalking2.png"
        visible: false
        mirror:player.mirror
    }
    Image{
        id: unarmedHand
        x:mirror?parent.width/2:7;
        y:parent.height/2
        source: "Images/unarmedHand.png"
        visible: true
        mirror:parent.mirror

        NumberAnimation on x {
                  loops: Animation.Infinite
                  from: mirror?24:10;
                  to: mirror?20:14;
                  duration: 500;
                  paused: !isWalking();
              }
    }
    Image{
        id: armedHand
        x:mirror?parent.width/2:7;
        y:parent.height/2
        source: "Images/armedHand.png"
        visible: false
        mirror:parent.mirror

        NumberAnimation on x {
            id:attackAnim1
            from: mirror?30:0
            to:mirror?20:10;
            duration: 100;
            running: false;
            alwaysRunToEnd: true;
        }
    }

    focus: true
    rotation: 0
    property int velocity: 4
    property bool goRight: false
    property bool goLeft: false
    property bool goUp: false
    property bool goDown: false

    function isWalking(){
        return goRight||goLeft||goUp||goDown;
    }

    function killingDetection(){
        for(var c = 0; c<scenario.children.length;c++){
            var child=scenario.children[c];
            if(child.toString().substring(0,3)==="NPC")
            if(x+width>child.x&&x<child.x+child.width&&
                    y+height>child.y&&y<child.y+child.height)
                child.alive=false;
        }
    }

    function attack(){
        attackAnim1.start();
        killingDetection();
    }

    Timer
    {
        interval: 20
        running: true
        repeat: true
        property int count:0
        onTriggered: {
            x+=goRight?x<scenario.width-width?!scenario.colisionDetection(x+5+velocity,y+40,30,5)?velocity:0:0:0;
            x-=goLeft?x>0?!scenario.colisionDetection(x+5-velocity,y+40,30,5)?velocity:0:0:0;
            y+=goDown?y<scenario.height-height?!scenario.colisionDetection(x+5,y+height-5+velocity,30,5)?velocity:0:0:0;
            y-=goUp?y>0?!scenario.colisionDetection(x+5,y+height-5-velocity,30,5)?velocity:0:0:0;
            mirror=goRight?true:mirror;
            mirror=goLeft?false:mirror;
            count=count<velocity*5-1?count+1:0;
            if(count%(velocity*5)>velocity*5/2)
            {
                walking1.visible=isWalking();
                walking2.visible=false;
            }
            else
            {
                walking1.visible=false;
                walking2.visible=isWalking();
            }
            z=scenario.priorityDetection(x+5,y+height-5,30,5)?0:2;
        }
    }

    Keys.onPressed:
    {
        goRight=event.key===Qt.Key_Right?true:goRight;
        goLeft=event.key===Qt.Key_Left?true:goLeft;
        goUp=event.key===Qt.Key_Up?true:goUp;
        goDown=event.key===Qt.Key_Down?true:goDown;
        mirror=event.key===Qt.Key_Right?true:mirror;
        mirror=event.key===Qt.Key_Left?false:mirror;

        switch(event.key){
            case Qt.Key_Space:
                if(armedHand.visible)
                    attack();
                else
                    for(var c = 0; c<scenario.children.length;c++){
                        var child=scenario.children[c];
                        if(child.toString().substring(0,10)==="PrisonGate")
                        if(x+width>child.x&&x<child.x+child.width&&
                                y+height>child.y&&y<child.y+child.height)
                            child.open();
            }
            break;
            case Qt.Key_W:
                armedHand.visible=!armedHand.visible;
                unarmedHand.visible=!armedHand.visible;
            break;
        }
    }

    Keys.onReleased:
    {
        goRight=event.key===Qt.Key_Right?false:goRight;
        goLeft=event.key===Qt.Key_Left?false:goLeft;
        goUp=event.key===Qt.Key_Up?false:goUp;
        goDown=event.key===Qt.Key_Down?false:goDown;
    }
}
